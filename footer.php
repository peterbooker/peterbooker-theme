<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package Kebo
 */
?>

	</div><!-- #main .row -->

        <div class="footer-area">
        
            <footer id="colophon" class="site-footer row" role="contentinfo">

                <div class="footer-content small-12 large-12 columns">

                    <?php get_sidebar('footer'); ?>

                </div>

            </footer><!-- #colophon .row -->
            
            <div class="site-info">
                Copyright © Peter Booker 2011-<?php echo date('Y'); ?>. All Rights Reserved.
                Powered by <a href="http://kebopowered.com/" title="<?php esc_attr_e( 'Kebo - Empowering People', 'pb-text' ); ?>" rel="generator"><?php _e( 'Kebo', 'pb-text' ); ?></a>
                <?php do_action( 'pb_credits' ); ?>

            </div><!-- .site-info .small-12 .large-12 -->
        
        </div><!-- .footer-area -->
        
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>