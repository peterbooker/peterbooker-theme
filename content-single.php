<?php
/**
 * The Template for displaying the content of posts.
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <header class="entry-header">

        <h1 class="entry-title"><?php the_title(); ?></h1>

        <div class="entry-meta">
            <?php pb_posted_on(); ?>
        </div><!-- .entry-meta -->

    </header><!-- .entry-header -->

    <div class="entry-content">
        <?php the_content(); ?>
        <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'kebo'), 'after' => '</div>')); ?>
    </div><!-- .entry-content -->
    
    <ul class="share-links">
        
        <li><a class="twitter" href="javascript:void(window.open('http://twitter.com/share?text=Check+out+this+article+on+<?php echo str_replace(' ', '+', get_the_title()); ?>&amp;url=<?php the_permalink(); ?>', 'twitter', 'width=600, height=400'));" title="Click to share this post on Twitter"><i class="icon-twitter"></i>Twitter</a></li>
        <li><a class="facebook" href="javascript:void(window.open('http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&t=<?php the_title(); ?>', 'facebook', 'width=600, height=400'));" title="Click to share this post on Facebook" data-title="<?php the_title(); ?>"><i class="icon-facebook"></i>Facebook</a></li>
        <li><a class="googleplus" href="javascript:void(window.open('https://plus.google.com/share?url=<?php the_permalink(); ?>', 'googleplus', 'width=600, height=400'));" title="Click to share this post on Google+"><i class="icon-google-plus"></i>Google+</a></li>
        <li><a class="linkedin" href="javascript:void(window.open('http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>&summary=<?php echo wp_trim_words(strip_tags(get_the_content($post->ID)), 50); ?>&source=<?php echo get_bloginfo('name'); ?>', 'linkedin', 'width=600, height=400'));" title="Click to share this post on LinkedIn"><i class="icon-linkedin"></i>LinkedIn</a></li>
        
    </ul>
    
    <footer class="entry-meta">
        <?php if ('post' == get_post_type()) : // Hide category and tag text for pages on Search ?>
            <?php
            /* translators: used between list items, there is a space after the comma */
            $categories_list = get_the_category_list(__(', ', 'pb-text'));
            if ($categories_list && pb_categorized_blog()) :
                ?>
                <span class="cat-links">
                    <?php printf(__('<i class="icon-folder-open" title="Categories"></i> %1$s', 'pb-text'), $categories_list); ?>
                </span>
            <?php endif; // End if categories ?>

            <?php
            /* translators: used between list items, there is a space after the comma */
            $tags_list = get_the_tag_list('', __(', ', 'pb-text'));
            if ($tags_list) :
                ?>
                <span class="tags-links">
                    <?php printf(__('<i class="icon-tags" title="Tags"></i> %1$s', 'pb-text'), $tags_list); ?>
                </span>
            <?php endif; // End if $tags_list ?>
        <?php endif; // End if 'post' == get_post_type() ?>

        <?php if (!post_password_required() && ( comments_open() || '0' != get_comments_number() )) : ?>
            <span class="comments-link"><i class="icon-comment"></i> <?php comments_popup_link(__('Leave a comment', 'pb-text'), __('1 Comment', 'pb-text'), __('% Comments', 'pb-text')); ?></span>
        <?php endif; ?>
            
        <span class="article-link"><i class="icon-link"></i> <a href="<?php the_permalink(); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark">Permalink</a></span>

        <?php edit_post_link(__('Edit', 'kebo'), '<span class="edit-link"><i class="icon-cog"></i> ', '</span>'); ?>
    </footer><!-- .entry-meta -->
    
</article><!-- #post-<?php the_ID(); ?> -->
