<?php

/**
 * Kebo functions and definitions
 */

if (!defined('PB_THEME_VERSION'))
    define('PB_THEME_VERSION', '0.6.5');

if (!function_exists('pb_setup')) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which runs
     * before the init hook. The init hook is too late for some features, such as indicating
     * support post thumbnails.
     */
    function pb_setup() {

        /**
         * Custom Kebo Framework files.
         */
        require( get_template_directory() . '/inc/custom-tags.php' );

        /**
         * Custom template tags for this theme.
         */
        require( get_template_directory() . '/inc/template-tags.php' );

        /**
         * Custom functions that act independently of the theme templates
         */
        require( get_template_directory() . '/inc/extras.php' );

        /**
         * Add default posts and comments RSS feed links to head
         */
        add_theme_support('automatic-feed-links');

        /*
         * This theme supports all available post formats.
         * See http://codex.wordpress.org/Post_Formats
         *
         * Structured post formats are formats where Twenty Thirteen handles the
         * output instead of the default core HTML output.
         */
        // Un-comment for WP 3.6 release
        //add_theme_support('structured-post-formats', array(
        //'link', 'video'
        //));
        
        add_theme_support('post-formats', array(
            'aside', 'audio', 'chat', 'gallery', 'image', 'quote', 'status'
        ));

        /**
         * This theme uses wp_nav_menu() in one location.
         */
        register_nav_menus(array(
            'primary' => __('Primary Menu', 'kebo'),
            'footer_credits' => __('Footer Credits', 'kebo'),
        ));

        /**
         * Enable support for Post Thumbnails
         */
        add_theme_support('post-thumbnails');

        /*
         * This theme uses a custom image size for featured images, displayed on
         * "standard" posts and pages.
         */
        set_post_thumbnail_size(604, 270, true);
    }

endif; // pb_setup
add_action('after_setup_theme', 'pb_setup');

/**
 * Register widgetized area and update sidebar with default widgets
 */
function pb_widgets_init() {
    
    register_sidebar(array(
        'name' => __('Sidebar', 'kebo'),
        'id' => 'sidebar-1',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h1 class="widget-title">',
        'after_title' => '</h1>',
    ));
    register_sidebar(array(
        'name' => __('Footer - Left', 'kebo'),
        'id' => 'sidebar-3',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h1 class="widget-title">',
        'after_title' => '</h1>',
    ));
    register_sidebar(array(
        'name' => __('Footer - Center', 'kebo'),
        'id' => 'sidebar-4',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h1 class="widget-title">',
        'after_title' => '</h1>',
    ));
    
}
add_action('widgets_init', 'pb_widgets_init');

/**
 * Enqueue scripts and styles
 */
function pb_scripts() {

    wp_enqueue_style('style', get_stylesheet_uri());
    
    // Queues the Theme css and less.
    wp_enqueue_style('theme-style', get_template_directory_uri() . '/css/app.css', array(), PB_THEME_VERSION, 'all');
    
    wp_enqueue_script('kebo-navigation', get_template_directory_uri() . '/js/navigation.js', array(), PB_THEME_VERSION, true);
    
    // Queues the Custom Modernizr file from Foundation.
    wp_enqueue_script('kebo-modernizr-js', get_template_directory_uri() . '/js/vendor/custom.modernizr.js', array(), PB_THEME_VERSION, false);
    
    // Main Foundation App js, must come last, starts the other plugins.
    wp_enqueue_script('kebo-foundation-js', get_template_directory_uri() . '/js/foundation.min.js', array('jquery'), PB_THEME_VERSION, false);

    if (is_singular() && comments_open() && get_option('thread_comments'))
        wp_enqueue_script('comment-reply');

    if (is_singular() && wp_attachment_is_image())
        wp_enqueue_script('kebo-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array('jquery'), '20120202', true);
    
}
add_action('wp_enqueue_scripts', 'pb_scripts');

/*
 * Add the Foundation init JS to Footer.
 */
function pb_insert_foundation_js() {
    
    if ( !is_page('home') ) {
    echo '
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $(document).foundation(function (response) {
                    console.log(response.errors);
                });
            });
        </script>
        ';
    }
    
}
add_action('wp_footer', 'pb_insert_foundation_js');

/**
 * Change the Excerpt output.
 */
function pb_excerpt_more($more) {
    return '...';
}
add_filter('excerpt_more', 'pb_excerpt_more');

/**
 * Sends blank searches to Search Page instead of Home Page.
 */
function pb_empty_search_fix($query_vars) {
    if (isset($_GET['s']) && empty($_GET['s'])) {
        $query_vars['s'] = " ";
    }
    return $query_vars;
}
add_filter('request', 'pb_empty_search_fix');
