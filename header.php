<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package Kebo
 */
?>
<!DOCTYPE html>
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
    <html <?php language_attributes(); ?>>
        <head>
            <meta charset="<?php bloginfo('charset'); ?>" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            
            <title><?php wp_title('|', true, 'right'); ?></title>
            
            <link rel="profile" href="http://gmpg.org/xfn/11" />
            <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
            <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/imgs/favicon.png" />

            <?php wp_head(); ?>
            
        </head>

        <body <?php body_class(); ?>>
            
            <script>
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                ga('create', 'UA-41385724-3', 'peterbooker.com');
                ga('send', 'pageview');
            </script>
            
            <div id="wrapper" class="hfeed site">
                
                <?php do_action('before'); ?>
                
                <div class="header-area">

                    <header id="masthead" class="site-header row" role="banner">

                        <div class="site-branding small-8 small-centered large-4 large-uncentered columns">
                            <h1 class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
                            <h2 class="site-description"><?php bloginfo('description'); ?></h2>
                        </div>

                        <nav id="site-navigation" class="site-navigation navigation-main small-12 large-8 columns" role="navigation">
                            <h1 class="menu-toggle"><?php _e('Menu', 'kebo'); ?></h1>
                            <div class="screen-reader-text skip-link"><a href="#content" title="Skip to content">Skip to content</a></div>
                            <?php
                            $menu = array(
                                'theme_location' => 'primary',
                                'menu' => '',
                                'container' => 'div',
                                'container_class' => '',
                                'container_id' => '',
                                'menu_class' => 'menu',
                                'menu_id' => '',
                                'echo' => true,
                                'fallback_cb' => 'wp_page_menu',
                                'before' => '',
                                'after' => '',
                                'link_before' => '',
                                'link_after' => '',
                                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                'depth' => 0,
                                'walker' => ''
                            );
                            ?>
                            <?php wp_nav_menu( $menu ); ?>
                        </nav><!-- #site-navigation -->

                    </header><!-- #masthead -->
                
                </div><!-- .header-area -->

                <div id="main" class="site-main row">
