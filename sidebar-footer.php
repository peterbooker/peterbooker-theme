<?php
/**
 * The Footer widget areas.
 *
 */
/* The footer widget area is triggered if any of the areas
 * have widgets. So let's check that first.
 *
 * If none of the sidebars have widgets, then let's bail early.
 */
if (!is_active_sidebar('sidebar-3') && !is_active_sidebar('sidebar-4') && !is_active_sidebar('sidebar-5')
)
    return;
// If we get this far, we have widgets. Let do this.
?>
<div class="footer-widgets row">
    <?php if (is_active_sidebar('sidebar-3')) : ?>
        <div id="first" class="widget-area small-8 small-centered large-4 large-uncentered columns" role="complementary">
            <?php dynamic_sidebar('sidebar-3'); ?>
        </div><!-- #first .widget-area -->
    <?php endif; ?>

    <?php if (is_active_sidebar('sidebar-4')) : ?>
        <div id="second" class="widget-area small-8 small-centered large-4 large-uncentered columns" role="complementary">
            <?php dynamic_sidebar('sidebar-4'); ?>
        </div><!-- #second .widget-area -->
    <?php endif; ?>

    <div id="third" class="widget-area small-8 small-centered large-4 large-uncentered columns" role="complementary">
        <aside class="widget social-widget">

            <h1 class="widget-title">Social</h1>

            <ul>
                <li><a href="https://twitter.com/peter_booker" target="_blank" title="Follow me on Twitter"><i class="icon-twitter"></i></a></li>
                <li><a href="https://github.com/PeterBooker" target="_blank" title="Follow me on Github"><i class="icon-github"></i></a></li>
                <li><a href="https://plus.google.com/107773915937223971155/?rel=author" target="_blank" title="Follow me on Google+"><i class="icon-google-plus"></i></a></li>
                <li><a href="http://www.linkedin.com/profile/view?id=182122980" target="_blank" title="Follow me on LinkedIn"><i class="icon-linkedin"></i></a></li>
            </ul>

        </aside>
    </div><!-- #third .widget-area -->
</div>