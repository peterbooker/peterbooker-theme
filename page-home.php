<?php
/**
 * The template for the Home page.
 */
get_header();
?>

<div id="primary" class="content-area small-12 large-8 columns">

    <div id="content" class="site-content" role="main">

        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <div class="entry-content">

                <p class="introduction">Welcome, my name is Peter Booker, I develop online products and services for the <a href="http://wordpress.org" title="WordPress - The most popular CMS" target="_blank">WordPress</a> platform. I am really excited to have set the new design of my website live. I am starting from scratch content wise so please bare with me as I flesh some of the areas out.</p>
                
                <ul class="small-block-grid-1 large-block-grid-2 boxes">
                    <li class="about">
                        <a href="<?php bloginfo( 'url' ); ?>/about/">
                            <div class="box">
                                <i class="icon-user"></i>
                                <h3 class="title">About</h3>
                                <p>Find out who I am.</p>
                            </div>
                        </a>
                    </li>
                    <li class="blog">
                        <a href="<?php bloginfo( 'url' ); ?>/blog/">
                            <div class="box">
                                <i class="icon-book"></i>
                                <h3 class="title">Blog</h3>
                                <p>Read some of my thoughts.</p>
                            </div>
                        </a>
                    </li>
                    <li class="kebo">
                        <a href="<?php bloginfo( 'url' ); ?>/kebo/">
                            <div class="box">
                                <i class="icon-building"></i>
                                <h3 class="title">My Business</h3>
                                <p>Coming soon.</p>
                            </div>
                        </a>
                    </li>
                    <li class="projects">
                        <a href="<?php bloginfo( 'url' ); ?>/projects/">
                            <div class="box">
                                <i class="icon-th"></i>
                                <h3 class="title">Projects</h3>
                                <p>Coming soon.</p>
                            </div>
                        </a>
                    </li>
                </ul>

            </div><!-- .entry-content -->
            <?php edit_post_link(__('Edit', 'kebo'), '<footer class="entry-meta"><span class="edit-link"><i class="icon-cog"></i> ', '</span></footer>'); ?>

        </article><!-- #post-<?php the_ID(); ?> -->

        <?php
        // If comments are open or we have at least one comment, load up the comment template
        if (comments_open() || '0' != get_comments_number())
            comments_template();
        ?>

    </div><!-- #content -->

</div><!-- #primary .small-12 .large-8 .columns -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
