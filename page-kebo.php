<?php
/**
 * The template for the Kebo page.
 */
get_header();
?>

<div id="primary" class="content-area small-12 large-8 columns">

    <div id="content" class="site-content" role="main">

        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            
            <header class="entry-header">
                <h1 class="entry-title"><?php the_title(); ?></h1>
            </header><!-- .entry-header -->
            
            <div class="entry-content">
                
                <p>Coming soon...</p>

            </div><!-- .entry-content -->
            <?php edit_post_link(__('Edit', 'kebo'), '<footer class="entry-meta"><span class="edit-link">', '</span></footer>'); ?>

        </article><!-- #post-<?php the_ID(); ?> -->

        <?php
        // If comments are open or we have at least one comment, load up the comment template
        if (comments_open() || '0' != get_comments_number())
            comments_template();
        ?>

    </div><!-- #content -->

</div><!-- #primary .small-12 .large-8 .columns -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
