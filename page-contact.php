<?php
/**
 * The template for the Projects page.
 */
get_header();
?>

<div id="primary" class="content-area small-12 large-8 columns">

    <div id="content" class="site-content" role="main">

        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <header class="entry-header">
                <h1 class="entry-title"><?php the_title(); ?></h1>
            </header><!-- .entry-header -->
            
            <div class="entry-content">

                <form id="form_fwftq9" method="post" enctype="multipart/form-data" data-abide>
                    
                    <div class="name-field">
                        <label for="field_name">Name <small>required</small></label>
                        <input id="field_name" name="item_meta[8]" type="text" required>
                        <small class="error">A name is required.</small>
                    </div>
                    
                    <div class="email-field">
                        <label for="field_email">Email <small>required</small></label>
                        <input id="field_email" name="item_meta[9]" type="email" required>
                        <small class="error">A valid email address is required.</small>
                    </div>
                    
                    <div class="email-field" style="margin-bottom: 20px;">
                        <label for="field_message">Message <small>required</small></label>
                        <textarea id="field_message" name="item_meta[10]" type="text" required></textarea>
                        <small class="error">A message is required.</small>
                    </div>
                    
                    <input type="hidden" value="create" name="frm_action">
                    <input type="hidden" value="2" name="form_id">
                    <input type="hidden" value="fwftq9" name="form_key">
                    <button id="submit" type="submit">Submit</button>
                    <input type="hidden" value="" name="item_key">
                    
                </form>

            </div><!-- .entry-content -->
            <?php edit_post_link(__('Edit', 'kebo'), '<footer class="entry-meta"><span class="edit-link">', '</span></footer>'); ?>

        </article><!-- #post-<?php the_ID(); ?> -->

        <?php
        // If comments are open or we have at least one comment, load up the comment template
        if (comments_open() || '0' != get_comments_number())
            comments_template();
        ?>

    </div><!-- #content -->

</div><!-- #primary .small-12 .large-8 .columns -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
