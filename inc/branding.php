<?php
/**
 * Custom Branded Control Panel
 */

if (!function_exists('pb_login_scripts')):

    /**
     * Enqueue Custom Login Script
     */
    function pb_login_scripts() {

        // Queues the custom login CSS file.
        wp_enqueue_style('login-style', get_template_directory_uri() . '/css/login.css');
        
    }

    add_action('login_head', 'pb_login_scripts');
    
endif; // pb_login_scripts

if (!function_exists('pb_admin_scripts')):

    /**
     * Enqueue Custom Admin Script
     */
    function pb_admin_scripts() {

        if (is_admin()) {
            
            // Queues the custom admin CSS file.
            wp_enqueue_style('style-admin', get_template_directory_uri() . '/css/admin.css');
            
        }
    }

    add_action('admin_enqueue_scripts', 'pb_admin_scripts');
    
endif; // pb_admin_scripts

if (!function_exists('pb_login_logo_url')):

    /**
     * Edit the Login Logo Link URL
     */
    function pb_login_logo_url($url) {

        return 'http://kebopowered.com';
    }

    add_filter('login_headerurl', 'pb_login_logo_url');
    
endif; // pb_login_logo_url